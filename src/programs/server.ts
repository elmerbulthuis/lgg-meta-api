import { isAbortError, waitForAbort } from "abort-tools";
import { program } from "commander";
import delay from "delay";
import * as http from "http";
import { second } from "msecs";
import * as prom from "prom-client";
import { WaitGroup } from "useful-wait-group";
import * as application from "../application/index.js";

program.
    command("server").
    option("--port <number>", "Listen to port", Number, 8080).
    option(
        "--endpoint <url>",
        "endpoint of the service",
        v => new URL(v),
        new URL("http://localhost:8080"),
    ).
    option("--shutdown-delay <msec>", "", Number, 35 * second).
    action(programAction);

interface ActionOptions {
    port: number;
    endpoint: URL;
    pgUri: URL;
    shutdownDelay: number;
}
async function programAction(actionOptions: ActionOptions) {
    const {
        port,
        endpoint,
        shutdownDelay,
    } = actionOptions;

    console.log("starting...");

    process.
        on("uncaughtException", error => {
            console.error(error);
            process.exit(1);
        }).
        on("unhandledRejection", error => {
            console.error(error);
            process.exit(1);
        });

    const onWarn = (error: unknown) => {
        console.warn(error);
    };

    const livenessController = new AbortController();
    const readinessController = new AbortController();

    {
        /*
        When we receive a signal, the server should be not ready anymore, so
        we abort the readiness controller
        */
        const onSignal = (signal: NodeJS.Signals) => {
            console.log(signal);
            readinessController.abort();
        };
        process.addListener("SIGINT", onSignal);
        process.addListener("SIGTERM", onSignal);
    }

    const promRegistry = new prom.Registry();

    const config: application.Config = {
        endpoint,

        livenessController,
        readinessController,

        promRegistry,
    };

    const applicationContext = application.createContext(config);

    /*
    We use the waitgroup to wait for requests to finish
    */
    const waitGroup = new WaitGroup();

    const applicationServer = application.createServer(
        applicationContext,
        onWarn,
    );
    applicationServer.registerMiddleware(
        (route, request, next) => waitGroup.with(() => next(request)),
    );

    const applicationHttpServer = http.createServer(applicationServer.asRequestListener({
        onError: onWarn,
    }));

    await new Promise<void>(resolve => applicationHttpServer.listen(
        port,
        () => resolve(),
    ));
    try {
        console.log("started");

        try {
            await waitForAbort(readinessController.signal, "application aborted");
        }
        catch (error) {
            if (!isAbortError(error)) throw error;
        }

        console.log("stopping...");

        /*
        wait a bit, usually for the load balancer to recognize this service as
        not ready and move traffic to another service.
        TODO: implement heartbeat so this will actually work!
        */
        await delay(shutdownDelay);

        /*
        wait for all requests to finish
        */
        await waitGroup.wait();

        /*
        abort the liveness controller to terminate all streaming responses!
        */
        livenessController.abort();

    }
    finally {
        await new Promise<void>((resolve, reject) => applicationHttpServer.close(
            error => error ?
                reject(error) :
                resolve(),
        ));
    }

    console.log("stopped");
}
