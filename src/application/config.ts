import * as prom from "prom-client";

export interface Config {
    endpoint: URL;

    readinessController: AbortController,
    livenessController: AbortController,

    promRegistry: prom.Registry;
}
