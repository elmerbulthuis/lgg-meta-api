import * as metaApi from "@latency.gg/lgg-meta-oas";
import * as oas3ts from "@oas3/oas3ts-lib";
import { Context } from "./context.js";

export type Server = metaApi.Server;

export function createServer(
    context: Context,
    onError: (error: unknown) => void,
) {
    const server = new metaApi.Server({});

    server.registerMiddleware(oas3ts.createErrorMiddleware(onError));

    server.registerLivenessOperation(() => {
        if (context.signals.liveness.aborted) {
            return {
                status: 503,
                parameters: {},
            };
        }

        return {
            status: 204,
            parameters: {},
        };
    });

    server.registerReadinessOperation(() => {
        if (context.signals.readiness.aborted) {
            return {
                status: 503,
                parameters: {},
            };
        }

        return {
            status: 204,
            parameters: {},
        };
    });

    server.registerMetricsOperation(() => {
        return {
            status: 200,
            parameters: {},
            async value() {
                const value = await context.config.promRegistry.metrics();
                return value;
            },
        } as const;
    });

    return server;
}
